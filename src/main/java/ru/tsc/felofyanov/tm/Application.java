package ru.tsc.felofyanov.tm;

import ru.tsc.felofyanov.tm.constant.ArgumentConst;
import ru.tsc.felofyanov.tm.constant.TerminalConst;

import java.util.Scanner;

public final class Application {

    public static void main(String[] args) {
        if (processArgument(args)) System.exit(0);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    public static void close() {
        System.exit(0);
    }

    public static boolean processArgument(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void showWelcome() {
        System.out.println("** WELCOME TO TASK-MANAGER **");
    }

    public static void showErrorArgument(String arg) {
        System.err.printf("Error! This argument '%s' not supported...\n", arg);
    }

    public static void showErrorCommand(String arg) {
        System.err.printf("Error! This command '%s' not supported...\n", arg);
    }

    public static void showAbout() {
        System.out.println("[About]");
        System.out.println("Name: Alexander Felofyanov");
        System.out.println("E-mail: afelofyanov@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("1.6.0");
    }

    public static void showHelp() {
        System.out.println("[Help]");
        System.out.format("%s, %s - Show developer info. \n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.format("%s, %s - Show applicant version. \n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.format("%s, %s - Show terminal commands info. \n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.format("%s - Close application. \n", TerminalConst.EXIT);
    }
}
